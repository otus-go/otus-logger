package logger

import (
	"bytes"
	"os"
	"testing"
	"time"
)

// TestMain - mock now function
func TestMain(m *testing.M) {
	now = func() time.Time {
		t, _ := time.Parse("2006-01-02", "2019-12-15")
		return t
	}
	code := m.Run()
	now = time.Now
	os.Exit(code)
}

func TestHwSubmitted_Summary(t *testing.T) {
	hw := HwSubmitted{Id: 1234, Comment: "please take a look at my homework"}
	if hw.Summary() != "submitted 1234 \"please take a look at my homework\"" {
		t.Error("HwSubmitted_Summary doesn't work correctly")
	}
}

func TestHwAccepted_Summary(t *testing.T) {
	hw := HwAccepted{Id: 1234, Grade: 10}
	if hw.Summary() != "accepted 1234 10" {
		t.Error("HwAccepted_Summary doesn't work correctly")
	}
}

func TestLogOtusHwAcceptedEvent(t *testing.T) {
	w := new(bytes.Buffer)
	e := HwAccepted{Id: 5678, Grade: 11}
	_ = LogOtusEvent(e, w)
	if w.String() != "2019-12-15 accepted 5678 11\n" {
		t.Errorf("Wrong logging message: %s", w.String())
	}
}
