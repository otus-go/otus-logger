package logger

import (
	"fmt"
	"io"
	"os"
	"time"
)

const dateFormat = "2006-01-02"

var now = time.Now // for mocking purposes

type OtusEvent interface {
	Summary() string
}

// HwAccepted - homework accepted
type HwAccepted struct {
	Id    int
	Grade int
}

func (h HwAccepted) Summary() string {
	return fmt.Sprintf("accepted %d %d", h.Id, h.Grade)
}

// HwSubmitted - homework submitted for review
type HwSubmitted struct {
	Id      int
	Code    string
	Comment string
}

func (h HwSubmitted) Summary() string {
	return fmt.Sprintf("submitted %d \"%s\"", h.Id, h.Comment)
}

// LogOtusEvent - write event summary into Writer
func LogOtusEvent(e OtusEvent, w io.Writer) error {
	if _, err := fmt.Fprintf(w, "%s %s\n", now().Format(dateFormat), e.Summary()); err != nil {
		return err
	}
	return nil
}

// Log - wrapper over LogOtusEvent for writing in stdout
func Log(e OtusEvent) error {
	return LogOtusEvent(e, os.Stdout)
}
